" vim: softtabstop=0 noexpandtab foldmethod=marker
" SPDX-License-Identifier: Apache-2.0 OR CC-BY-4.0 OR Vim
" Author: Mizuno Jan Yuta <mjy@8x9.space>


"""" Option Variables: {{{

let s:opt_varnames = []
function! s:def_opt(varname, default)
	if !exists(a:varname)
		let {a:varname} = a:default
	endif
	eval s:opt_varnames->add(a:varname)
endfunction

call s:def_opt('g:chalkthin_bg_colorless',   v:false)
call s:def_opt('g:chalkthin_diff_emph',      0)
call s:def_opt('g:chalkthin_comment_emph',   0)
call s:def_opt('g:chalkthin_folded_emph',    0)
call s:def_opt('g:chalkthin_normal_emph',    0)
call s:def_opt('g:chalkthin_statement_emph', 0)
call s:def_opt('g:chalkthin_visual_emph',    0)

function! Chalkthin_echo_opts()
	for l:n in s:opt_varnames
		echo printf('%-30s %s', l:n, {l:n})
	endfor
endfunction


"}}}
"""" Init Color Scheme: {{{

set background=dark
hi clear
if exists("syntax_on")
	syntax reset
endif
let g:colors_name = "chalkthin"


"}}}
"""" Util Funcs: {{{

function! s:hi(group, fg, bg, attrs='NONE', sp='NONE')
	execute 'hi' a:group
		\ 'guifg='a:fg 'guibg='a:bg
		\ 'gui='a:attrs 'cterm='a:attrs 'term='a:attrs
		\ 'guisp='a:sp
endfunction

function! s:linkto(to, ...)
	for l:from in a:000
		execute 'hi link' l:from a:to
	endfor
endfunction


"}}}
"""" Soft Colors: {{{

let s:salmon_pink_light  = '#dfacad'  " lch( 75.0%,  20.0,  20.0)
let s:salmon_pink_base   = '#e2979a'  " lch( 70.0%,  30.0,  20.0)
let s:salmon_pink_calm   = '#d19f9f'  " lch( 70.0%,  20.0,  20.0)
let s:salmon_pink_dim    = '#d38a8d'  " lch( 65.0%,  30.0,  20.0)
let s:salmon_pink_ash    = '#b77073'  " lch( 55.0%,  30.0,  20.0)
let s:salmon_pink_shade  = '#5e3e3f'  " lch( 30.0%,  15.0,  20.0)
let s:salmon_pink_dark   = '#332727'  " lch( 17.0%,   6.0,  20.0)
let s:salmon_pink_black  = '#211a1a'  " lch( 10.0%,   4.0,  20.0)

let s:orange_brown_light = '#d4b298'  " lch( 75.0%,  20.0,  65.0)
let s:orange_brown_base  = '#d2a27a'  " lch( 70.0%,  30.0,  65.0)
let s:orange_brown_calm  = '#c6a58b'  " lch( 70.0%,  20.0,  65.0)
let s:orange_brown_dim   = '#c3946e'  " lch( 65.0%,  30.0,  65.0)
let s:orange_brown_ash   = '#a77b55'  " lch( 55.0%,  30.0,  65.0)
let s:orange_brown_shade = '#574332'  " lch( 30.0%,  15.0,  65.0)
let s:orange_brown_dark  = '#312822'  " lch( 17.0%,   6.0,  65.0)
let s:orange_brown_black = '#201b17'  " lch( 10.0%,   4.0,  65.0)

let s:yellow_green_light = '#babb96'  " lch( 75.0%,  20.0, 110.0)
let s:yellow_green_base  = '#abaf77'  " lch( 70.0%,  30.0, 110.0)
let s:yellow_green_calm  = '#acae89'  " lch( 70.0%,  20.0, 110.0)
let s:yellow_green_dim   = '#9ea26b'  " lch( 65.0%,  30.0, 110.0)
let s:yellow_green_ash   = '#838752'  " lch( 55.0%,  30.0, 110.0)
let s:yellow_green_shade = '#474831'  " lch( 30.0%,  15.0, 110.0)
let s:yellow_green_dark  = '#2a2a22'  " lch( 17.0%,   6.0, 110.0)
let s:yellow_green_black = '#1c1c16'  " lch( 10.0%,   4.0, 110.0)

let s:summer_green_light = '#9bc2a9'  " lch( 75.0%,  20.0, 155.0)
let s:summer_green_base  = '#7cb893'  " lch( 70.0%,  30.0, 155.0)
let s:summer_green_calm  = '#8db49b'  " lch( 70.0%,  20.0, 155.0)
let s:summer_green_dim   = '#6faa86'  " lch( 65.0%,  30.0, 155.0)
let s:summer_green_ash   = '#56906d'  " lch( 55.0%,  30.0, 155.0)
let s:summer_green_shade = '#334c3c'  " lch( 30.0%,  15.0, 155.0)
let s:summer_green_dark  = '#232c26'  " lch( 17.0%,   6.0, 155.0)
let s:summer_green_black = '#171d19'  " lch( 10.0%,   4.0, 155.0)

let s:blue_green_light   = '#87c3c4'  " lch( 75.0%,  20.0, 200.0)
let s:blue_green_base    = '#56babd'  " lch( 70.0%,  30.0, 200.0)
let s:blue_green_calm    = '#7ab5b7'  " lch( 70.0%,  20.0, 200.0)
let s:blue_green_dim     = '#47acaf'  " lch( 65.0%,  30.0, 200.0)
let s:blue_green_ash     = '#249294'  " lch( 55.0%,  30.0, 200.0)
let s:blue_green_shade   = '#244d4e'  " lch( 30.0%,  15.0, 200.0)
let s:blue_green_dark    = '#1f2c2d'  " lch( 17.0%,   6.0, 200.0)
let s:blue_green_black   = '#151d1d'  " lch( 10.0%,   4.0, 200.0)

let s:sky_blue_light     = '#92bfd9'  " lch( 75.0%,  20.0, 245.0)
let s:sky_blue_base      = '#68b4dc'  " lch( 70.0%,  30.0, 245.0)
let s:sky_blue_calm      = '#84b1cb'  " lch( 70.0%,  20.0, 245.0)
let s:sky_blue_dim       = '#59a7ce'  " lch( 65.0%,  30.0, 245.0)
let s:sky_blue_ash       = '#3a8cb2'  " lch( 55.0%,  30.0, 245.0)
let s:sky_blue_shade     = '#2b4a5b'  " lch( 30.0%,  15.0, 245.0)
let s:sky_blue_dark      = '#212b32'  " lch( 17.0%,   6.0, 245.0)
let s:sky_blue_black     = '#161c20'  " lch( 10.0%,   4.0, 245.0)

let s:cold_purple_light  = '#b4b6db'  " lch( 75.0%,  20.0, 290.0)
let s:cold_purple_base   = '#a2a7de'  " lch( 70.0%,  30.0, 290.0)
let s:cold_purple_calm   = '#a6a9cd'  " lch( 70.0%,  20.0, 290.0)
let s:cold_purple_dim    = '#959ad0'  " lch( 65.0%,  30.0, 290.0)
let s:cold_purple_ash    = '#7a80b4'  " lch( 55.0%,  30.0, 290.0)
let s:cold_purple_shade  = '#43455c'  " lch( 30.0%,  15.0, 290.0)
let s:cold_purple_dark   = '#292932'  " lch( 17.0%,   6.0, 290.0)
let s:cold_purple_black  = '#1b1b20'  " lch( 10.0%,   4.0, 290.0)

let s:pink_purple_light  = '#d3aec9'  " lch( 75.0%,  20.0, 335.0)
let s:pink_purple_base   = '#d29ac3'  " lch( 70.0%,  30.0, 335.0)
let s:pink_purple_calm   = '#c5a0bb'  " lch( 70.0%,  20.0, 335.0)
let s:pink_purple_dim    = '#c48db5'  " lch( 65.0%,  30.0, 335.0)
let s:pink_purple_ash    = '#a8739a'  " lch( 55.0%,  30.0, 335.0)
let s:pink_purple_shade  = '#573f51'  " lch( 30.0%,  15.0, 335.0)
let s:pink_purple_dark   = '#30272e'  " lch( 17.0%,   6.0, 335.0)
let s:pink_purple_black  = '#1f1a1e'  " lch( 10.0%,   4.0, 335.0)


"}}}
"""" Neutral Colors: {{{

let s:silver_shine       = '#e2e2e2'  " lch( 90.0%,   0.0,   0.0)
let s:silver_white       = '#cccccc'  " lch( 82.0%,   0.0,   0.0)
let s:silver_light       = '#b9b9b9'  " lch( 75.0%,   0.0,   0.0)
let s:silver_base        = '#ababab'  " lch( 70.0%,   0.0,   0.0)
let s:silver_dim         = '#9e9e9e'  " lch( 65.0%,   0.0,   0.0)
let s:silver_ash         = '#848484'  " lch( 55.0%,   0.0,   0.0)
let s:silver_gray        = '#5e5e5e'  " lch( 40.0%,   0.0,   0.0)
let s:silver_shade       = '#474747'  " lch( 30.0%,   0.0,   0.0)
let s:silver_dark        = '#2a2a2a'  " lch( 17.0%,   0.0,   0.0)
let s:silver_black       = '#1b1b1b'  " lch( 10.0%,   0.0,   0.0)


"}}}
"""" Strong Colors: {{{

let s:cosmos             = '#ffd9dc'  " lch( 90.0%, 134.0,  15.0)
let s:yellow             = '#f3e649'  " lch( 90.0%,  75.0, 100.0)
let s:green              = '#6fd000'  " lch( 75.0%, 120.0, 127.0)
let s:orange             = '#dfa000'  " lch( 70.0%,  80.0,  80.0)
let s:emerald            = '#20c0a5'  " lch( 70.0%,  45.0, 177.0)
let s:vermilion          = '#fd5838'  " lch( 60.0%,  80.0,  40.0)
let s:red                = '#ff0000'  " lch( 53.2%, 104.6,  40.0)
let s:wine               = '#5d0000'  " lch( 17.0%, 134.0,  40.0)


"}}}
"""" Customizable Colors: {{{

if g:chalkthin_bg_colorless
	let s:normal_bg        = s:silver_dark
	let s:color_column_bg  = s:silver_shade
	let s:cursor_column_bg = s:silver_black
	let s:cursor_line_bg   = s:silver_black
else
	" default: v:false
	let s:normal_bg        = s:blue_green_dark
	let s:color_column_bg  = s:blue_green_shade
	let s:cursor_column_bg = s:blue_green_black
	let s:cursor_line_bg   = s:blue_green_black
endif

if g:chalkthin_comment_emph > 0
	let s:comment_fg      = s:emerald
	let s:comment_note_fg = s:blue_green_calm
	let s:comment_out_fg  = s:blue_green_calm
	let s:comment_emph_fg = s:emerald
elseif g:chalkthin_comment_emph < 0
	let s:comment_fg      = s:silver_ash
	let s:comment_note_fg = s:blue_green_calm
	let s:comment_out_fg  = s:silver_ash
	let s:comment_emph_fg = s:blue_green_calm
else
	" default: 0
	let s:comment_fg      = s:blue_green_calm
	let s:comment_note_fg = s:blue_green_calm
	let s:comment_out_fg  = s:blue_green_calm
	let s:comment_emph_fg = s:blue_green_calm
endif

if g:chalkthin_statement_emph > 0
	let s:statement_fg = s:orange
elseif g:chalkthin_statement_emph < 0
	let s:statement_fg = s:yellow_green_ash
else
	" default: 0
	let s:statement_fg = s:yellow_green_base
endif

if g:chalkthin_visual_emph > 0
	let s:visual_fg       = s:silver_ash
	let s:visual_bg       = s:silver_black
	let s:visual_attr     = 'reverse'
	let s:visual_nos_fg   = s:silver_gray
	let s:visual_nos_bg   = s:silver_black
	let s:visual_nos_attr = 'reverse'
elseif g:chalkthin_visual_emph < 0
	let s:visual_fg       = 'NONE'
	let s:visual_bg       = s:silver_shade
	let s:visual_attr     = 'NONE'
	let s:visual_nos_fg   = s:silver_ash
	let s:visual_nos_bg   = s:silver_black
	let s:visual_nos_attr = 'NONE'
else
	" default: 0
	let s:visual_fg       = 'NONE'
	let s:visual_bg       = s:silver_gray
	let s:visual_attr     = 'NONE'
	let s:visual_nos_fg   = s:silver_dim
	let s:visual_nos_bg   = s:silver_shade
	let s:visual_nos_attr = 'NONE'
endif

if g:chalkthin_normal_emph > 0
	let s:normal_fg = s:silver_white
elseif g:chalkthin_normal_emph < 0
	let s:normal_fg = s:silver_base
else
	" default: 0
	let s:normal_fg = s:silver_light
endif

if g:chalkthin_folded_emph > 0
	let s:folded_fg   = s:blue_green_base
	let s:folded_bg   = s:blue_green_shade
	let s:folded_attr = 'bold'
elseif g:chalkthin_folded_emph < 0
	let s:folded_fg   = s:silver_ash
	let s:folded_bg   = s:blue_green_shade
	let s:folded_attr = 'NONE'
else
	" default: 0
	let s:folded_fg   = s:silver_dim
	let s:folded_bg   = s:blue_green_shade
	let s:folded_attr = 'bold'
endif

if g:chalkthin_diff_emph > 0
	let s:diff_add_fg    = s:sky_blue_light
	let s:diff_change_fg = s:normal_fg
	let s:diff_text_fg   = s:orange
	let s:diff_emph_attr = 'bold'
elseif g:chalkthin_diff_emph < 0
	let s:diff_add_fg    = 'NONE'
	let s:diff_change_fg = 'NONE'
	let s:diff_text_fg   = 'NONE'
	let s:diff_emph_attr = 'NONE'
else
	" default: 0
	let s:diff_add_fg    = 'NONE'
	let s:diff_change_fg = 'NONE'
	let s:diff_text_fg   = 'NONE'
	let s:diff_emph_attr = 'bold'
endif


"}}}
"""" Default Highlight Groups: {{{
"""" :help highlight-default

"-------- GROUP ---------- FG ------------------ BG ------------------ [ATTRS] [SP]
call s:hi('Normal',        s:normal_fg,          s:normal_bg)
call s:hi('ColorColumn',   'NONE',               s:color_column_bg)
call s:hi('Conceal',       s:silver_white,       s:silver_gray)
call s:hi('Cursor',        s:green,              'bg',                 'reverse')
call s:hi('CursorColumn',  'NONE',               s:cursor_column_bg)
call s:hi('CursorLine',    'NONE',               s:cursor_line_bg)
call s:hi('Directory',     s:sky_blue_base,      'NONE')
call s:hi('DiffAdd',       s:diff_add_fg,        s:sky_blue_shade,     s:diff_emph_attr)
call s:hi('DiffChange',    s:diff_change_fg,     s:summer_green_shade)
call s:hi('DiffDelete',    s:cold_purple_base,   s:cold_purple_dark,   s:diff_emph_attr)
call s:hi('DiffText',      s:diff_text_fg,       s:wine,               s:diff_emph_attr)
call s:hi('ErrorMsg',      s:salmon_pink_dim,    'NONE',               'reverse')
call s:hi('VertSplit',     s:silver_shade,       s:silver_shade,       'reverse')
call s:hi('Folded',        s:folded_fg,          s:folded_bg,           s:folded_attr)
call s:hi('FoldColumn',    s:silver_base,        s:silver_black)
call s:hi('SignColumn',    s:salmon_pink_light,  s:silver_shade)
call s:hi('IncSearch',     s:yellow,             'NONE',               'reverse,bold')
call s:hi('LineNr',        s:silver_gray,        s:silver_black)
"    omit  LineNrAbove
"    omit  LineNrBelow
call s:hi('CursorLineNr',  s:yellow_green_base,  s:silver_black,       'bold')
call s:hi('MatchParen',    s:yellow,             'bg',                 'underline,bold')
call s:hi('ModeMsg',       'NONE',               'NONE',               'bold')
call s:hi('MoreMsg',       s:summer_green_base,  'NONE',               'bold')
call s:hi('NonText',       s:silver_ash,         'NONE')
call s:hi('Pmenu',         'NONE',               s:blue_green_shade)
call s:hi('PmenuSel',      s:sky_blue_dim,       'NONE',               'reverse,bold')
call s:hi('PmenuSbar',     'NONE',               s:silver_shade,       'bold')
call s:hi('PmenuThumb',    s:silver_base,        'NONE',               'reverse')
call s:hi('Question',      s:summer_green_base,  'NONE',               'bold')
call s:hi('QuickFixLine',  'NONE',               'NONE',               'underline')
call s:hi('Search',        s:yellow,             'NONE',               'bold')
call s:hi('SpecialKey',    s:sky_blue_dim,       'NONE')
call s:hi('SpellBad',      'NONE',               'NONE',               'undercurl', s:red)
call s:hi('SpellCap',      'NONE',               'NONE',               'undercurl', s:cold_purple_base)
call s:hi('SpellLocal',    'NONE',               'NONE',               'undercurl', s:green)
call s:hi('SpellRare',     'NONE',               'NONE',               'undercurl', s:yellow)
call s:hi('StatusLine',    s:silver_light,       s:silver_black,       'reverse,bold')
call s:hi('StatusLineNC',  s:silver_ash,         s:silver_black,       'reverse')
call s:hi('TabLine',       s:silver_ash,         s:silver_black,       'reverse')
call s:hi('TabLineFill',   'NONE',               s:silver_shade)
call s:hi('TabLineSel',    s:silver_light,       s:silver_black,       'reverse,bold')
call s:hi('Terminal',      'NONE',               s:silver_shade)
call s:hi('Title',         s:orange,             'NONE')
call s:hi('Visual',        s:visual_fg,          s:visual_bg,          s:visual_attr)
call s:hi('VisualNOS',     s:visual_nos_fg,      s:visual_nos_bg,      s:visual_nos_attr)
call s:hi('WarningMsg',    s:cosmos,             'NONE')
call s:hi('WildMenu',      s:yellow,             'NONE',               'reverse,bold')
"    omit  Menu
"    omit  Scrollbar
"    omit  Tooltip
call s:linkto('Cursor',       'lCursor', 'CursorIM')
call s:linkto('StatusLine',   'StatusLineTerm')
call s:linkto('StatusLineNC', 'StatusLineTermNC')


"}}}
"""" Syntax Highlight Groups: {{{
""""   :help group-name

"-------- GROUP ---------- FG ------------------ BG ------------------ [ATTRS] [SP]
call s:hi('Comment',       s:comment_fg,         'NONE')
call s:hi('Constant',      s:sky_blue_base,      'NONE')
call s:hi('Identifier',    s:cold_purple_base,   'NONE')
call s:hi('Statement',     s:statement_fg,       'NONE')
call s:hi('PreProc',       s:salmon_pink_dim,    'NONE')
call s:hi('Special',       s:orange_brown_base,  'NONE')
call s:hi('Type',          s:pink_purple_base,   'NONE')
call s:hi('Underlined',    s:sky_blue_light,     'NONE',               'underline')
"    omit  Ignore
call s:hi('Error',         s:salmon_pink_base,   'NONE',               'reverse,bold')
call s:hi('Todo',          s:cosmos,             'NONE',               'bold')


"}}}
"""" Ad Hoc: {{{

" namespace for original highlight groups
let s:o = 'Chalkthin'

" original highlight groups
"-------- GROUP ---------- FG ------------------ BG ------------------ [ATTRS] [SP]
call s:hi(s:o.'Info',      s:orange,             'NONE')
call s:hi(s:o.'Warning',   s:cosmos,             'NONE')
call s:hi(s:o.'Error',     s:vermilion,          'NONE')
call s:hi(s:o.'Note',      s:blue_green_calm,    'NONE')

call s:hi(s:o.'ComNote',   s:comment_note_fg,    'NONE')
call s:hi(s:o.'ComOut',    s:comment_out_fg,     'NONE')
call s:hi(s:o.'ComEmph',   s:comment_emph_fg,    'NONE')

" Linked to 'Comment' by default 
call s:linkto(s:o.'ComNote',
	\ 'helpExample',
	\ 'helpCommand',
	\ )
call s:linkto(s:o.'ComOut',
	\ 'rubyData',
	\ 'perlDATA'
	\ )
call s:linkto(s:o.'ComEmph',
	\ 'vimScriptDelim',
	\ 'changelogFiles',
	\ 'changelogFuncs',
	\ )

" HTML, Markdown
call s:hi('htmlBold',      'NONE',               'NONE',               'bold')
call s:hi('htmlItalic',    'NONE',               'NONE',               'italic')
call s:linkto('Identifier',
	\ 'htmlTagName',
	\ 'htmlTag',
	\ 'htmlEndTag',
	\ )

" vim
"call s:linkto('Title', 'vimCommentTitle')

" go
call s:linkto('Normal', 'goSpaceError')

" haskell
" XXX

" vim-lsp
call s:hi('lspReference',  'NONE',               s:silver_gray)
call s:linkto('Visual',   'lspReference')
call s:linkto('Error',    'LspErrorHighlight', 'LspErrorText', 'LspErrorVirtualText')
call s:linkto('ErrorMsg',
	\ 'LspInformationHighlight',
	\ 'LspHintHighlight',
	\ 'LspInformationText',
	\ 'LspHintText',
	\ 'LspInformationVirtualText',
	\ 'LspHintVirtualText',
	\ 'LspCodeActionText',
	\ )

" coc
call s:linkto('Visual',      'CocHighlightText', 'CocHighlightRead', 'CocHighlightWrite')
call s:linkto('SpellRare',   'CocUnusedHighlight')
call s:linkto(s:o.'Info',    'CocInfoSign')
call s:linkto(s:o.'Warning', 'CocWarningSign')
call s:linkto(s:o.'Error',   'CocErrorSign')
call s:linkto(s:o.'Note',    'CocHintSign')


"}}}
